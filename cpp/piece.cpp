#include "piece.h" 

piece::piece(const jsoncons::json& data) { 
	type = data.has_member("length");
	if (type) {
		l = data["length"].as_double();
		r = 0.0;
		angle = 0.0;
	}
	else {
		l = 0.0;
		r = data["radius"].as_double();
		angle = data["angle"].as_double();
	}
	if (data.has_member("switch")) sw = data["switch"].as_bool();
	else sw = false;
}

piece::~piece() {}

bool piece::isStraight() { return type; }

bool piece::isSwitch() { return sw; }

double piece::getLength() { return l; }

double piece::getRadius() { return r; }

double piece::getAngle() { return angle; }