#ifndef HWO_PIECE_H
#define HWO_PIECE_H

#include <iostream>
#include <jsoncons/json.hpp>

class piece {
	private:
		// true = recta; false = corba
		bool type;
		double l;
		double r;
		double angle;
		bool sw;

	public:
		piece(const jsoncons::json& data);
		~piece();
		bool isStraight();
		bool isSwitch();
		double getLength();
		double getRadius();
		double getAngle();
};

#endif