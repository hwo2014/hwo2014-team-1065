#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <stack>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
#include "piece.h"
#include <cmath>
#include <queue>
#include <boost/math/constants/constants.hpp>

using namespace std;

typedef pair<double, int> PDI;
typedef vector<PDI> VPDI;
typedef vector<VPDI> Graf;
typedef priority_queue<PDI,vector<PDI>,greater<PDI> > minheap;

struct dades_solver1 {
    double vel_ant;
    double dist_ant;
    int peca_ant;
    double angle_ant;
};

struct dades_l{
    double firstAngle = 0;
    double secondAngle = 0;
    double thirdAngle = 0;
    int numVisites = 0;     //Evita que es calculi "l" abans que es tinguin totes les dades.
};

class game_logic
{
public:
    typedef std::vector<jsoncons::json> msg_vector;

    game_logic();
    msg_vector react(const jsoncons::json& msg);

private:
    typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
    const std::map<std::string, action_fun> action_map;

    const double pi = boost::math::constants::pi<double>();
    const double epsilon = 1e-5;
    string car_color;
    vector<piece> circuit;
    vector<double> lanes;
    double lambda;
    const double k = 10.0;
    Graf graf;
    double vmax, vmin;
    const double angle_lim = 60.0;
    double alpha;
    dades_solver1 ds1;
    dades_l dl;
    vector<double> velocitat;
    double throttle;
    int throttle_ticks;
    bool chooseSwitch;
    double l = 0;
    bool lTrobada = false;
    bool first;
    vector<vector<double>> velocitats;
    double turboDurationMilliseconds;
    int turboDurationTicks;
    double turboFactor;


    msg_vector on_join(const jsoncons::json& data);
    msg_vector on_join_race(const jsoncons::json& data);
    msg_vector on_your_car(const jsoncons::json& data);
    msg_vector on_game_init(const jsoncons::json& data);
    msg_vector on_game_start(const jsoncons::json& data);
    msg_vector on_car_positions(const jsoncons::json& data);
    msg_vector on_crash(const jsoncons::json& data);
    msg_vector on_spawn(const jsoncons::json& data);
    msg_vector on_game_end(const jsoncons::json& data);
    msg_vector on_error(const jsoncons::json& data);
    msg_vector on_turbo_available(const jsoncons::json& data);
    msg_vector on_turbo_start(const jsoncons::json& data);
    msg_vector on_turbo_end(const jsoncons::json& data);
    msg_vector on_lap_finished(const jsoncons::json& data);
    msg_vector on_dnf(const jsoncons::json& data);
    void dijkstra(const Graf& g, vector<double>& dist, vector<int>& cami, int s);
    double getVelocity(int peca_act, int lane_act, double dist_act);
    void calcula_velocitats_max();
    template <typename T> double sgn(T val);
};

#endif
