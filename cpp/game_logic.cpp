#include "game_logic.h"
#include "protocol.h"

using namespace std;
using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
        { "join", &game_logic::on_join },
        { "joinRace", &game_logic::on_join_race },
        { "yourCar", &game_logic::on_your_car },
        { "gameInit", &game_logic::on_game_init },
        { "gameStart", &game_logic::on_game_start },
        { "carPositions", &game_logic::on_car_positions },
        { "crash", &game_logic::on_crash },
        { "spawn", &game_logic::on_spawn },
        { "gameEnd", &game_logic::on_game_end },
        { "error", &game_logic::on_error },
        { "turboAvailable", &game_logic::on_turbo_available },
        { "turboStart", &game_logic::on_turbo_start },
        { "turboEnd", &game_logic::on_turbo_end },
        { "lapFinished", &game_logic::on_lap_finished },
        { "dnf", &game_logic::on_dnf },
        { "lapFinished", &game_logic::on_lap_finished }
    }
{
    ds1.vel_ant = 0.0;
    ds1.peca_ant = 0;
    ds1.dist_ant = 0.0;
    ds1.angle_ant = 0.0;
    lambda = 0.0;
    throttle = 0.6;
    throttle_ticks = 0;
    chooseSwitch = true;
    first = true;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg) {
    const auto& msg_type = msg["msgType"].as<std::string>();
    const auto& data = msg["data"];
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end()) {
        return (action_it->second)(this, data);
    }
    else {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_ping() };
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data) {
    //Entres a la partida
    std::cout << "Joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& data) {
    //Entres a la partida
    std::cout << "Race joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data) {
    // Informació bàsica del cotxe
    std::string color = data["color"].as<std::string>();
    std::cout << "Our color is: " << color << std::endl;
    car_color = color;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
    // Informació bàsica del circuit
    std::cout << "Race started" << std::endl;
    // Afegeix informació de cada peça del circuit
    jsoncons::json aux = data["race"]["track"]["pieces"];
    for (int i = 0; i < aux.size(); i++) {
        circuit.push_back(piece(aux[i]));
    }
    aux = data["race"]["track"]["lanes"];
    lanes = vector<double>(aux.size());
    for (int i = 0; i < aux.size(); i++) {
        lanes[aux[i]["index"].as_int()] = aux[i]["distanceFromCenter"].as_double();
    }
    graf = Graf(circuit.size()*lanes.size());
    int vertex = 0;
    for (int i = 0; i < circuit.size(); i++) {
        for (int j = 0; j < lanes.size(); j++) {
            int next_vertex = vertex + lanes.size();
            double dist;
            if (circuit[i].isStraight()) {
                dist = circuit[i].getLength();
            }
            else {
                if (circuit[i].getAngle() > 0) dist = (abs(circuit[i].getAngle())*pi/180.0)*(circuit[i].getRadius()-lanes[j]);
                else dist = (abs(circuit[i].getAngle())*pi/180.0)*(circuit[i].getRadius()+lanes[j]);
            }
            graf[vertex].push_back(PDI(dist, next_vertex%graf.size()));
            if (circuit[i].isSwitch()) {
                if (j == 0) {
                    graf[vertex].push_back(PDI(dist, (next_vertex+1)%graf.size()));
                }
                else if (j == lanes.size()-1) {
                    graf[vertex].push_back(PDI(dist, (next_vertex-1)%graf.size()));
                }
                else {
                    graf[vertex].push_back(PDI(dist, (next_vertex+1)%graf.size()));
                    graf[vertex].push_back(PDI(dist, (next_vertex-1)%graf.size()));
                }
            }
            vertex++;
        }
    }
    for (int i = 0; i < graf.size(); i++) {
        for (int j = 0; j < graf[i].size(); j++) {
            cout << "Vertex " << i << " Next vertex " << graf[i][j].second << " distance " << graf[i][j].first << endl;
        }
    }
    // Escriu informació de cada peça del circuit
    for (int i = 0; i < circuit.size(); i++) {
        std::cout << "Piece " << i << ":";
        if (circuit[i].isStraight()) {
            std::cout << " length = " << circuit[i].getLength();
        }
        else std::cout << " radius = " << circuit[i].getRadius() << " angle = " << circuit[i].getAngle();
        if (circuit[i].isSwitch()) std::cout << " and is a switch";
        std::cout << std::endl;
    }

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data) {
    // Comença la carrera
    std::cout << "Race started" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data) {
    // Es mou el cotxe
    //std::cout << "WOLOLO!!!" << std::endl;
    double throttle = 0.5;
    for (size_t it = 0; it < data.size(); ++it) {
        if (car_color == data[it]["id"]["color"].as_string()) { // DO AI THINGS: WOLOLO!!!!
            // Calcular velocitat
            jsoncons::json infoCotxe = data[it];
            int peca_act = infoCotxe["piecePosition"]["pieceIndex"].as_int();
            int lane = infoCotxe["piecePosition"]["lane"]["startLaneIndex"].as_int();
            int node_act = peca_act * lanes.size() + lane;
            int endLane = infoCotxe["piecePosition"]["lane"]["endLaneIndex"].as_int();
            double inPieceDist = infoCotxe["piecePosition"]["inPieceDistance"].as_double();
            double vel_act = getVelocity(peca_act, lane, inPieceDist);
            double angle = infoCotxe["angle"].as_double();

            int vertex_act = peca_act*lanes.size()+lane;

            //cout << endl;
            //cerr << peca_act << "\t" << inPieceDist << "\t" << throttle << "\t" << vel_act << "\t" << vel_act-ds1.vel_ant << "\t" << angle << endl;
            if (vel_act != 0 and lambda == 0.0) {
                lambda = -log(1.0-vel_act/k);
                cout << "lambda: " << lambda << ", " << k << endl;
            }

            // Controlar si no s'ha trobat "l" en el primer revolt.
            if (circuit[peca_act].isStraight() and dl.numVisites > 0) {
                dl.numVisites = 0;
            }
            else if (not circuit[peca_act].isStraight() and not lTrobada) {
                dl.firstAngle = dl.secondAngle;
                dl.secondAngle = dl.thirdAngle;
                dl.thirdAngle = angle;

                if (dl.firstAngle < dl.secondAngle and dl.secondAngle > dl.thirdAngle and dl.numVisites >= 2) {
                    l = ds1.vel_ant*ds1.vel_ant/(circuit[peca_act].getRadius()+(sgn(angle)*lanes[lane]));
                    calcula_velocitats_max();
                    lTrobada = true;
                }
                ++dl.numVisites;
            }

            if (circuit[peca_act].isSwitch()) chooseSwitch = true;

            // Wololorithm
            if (chooseSwitch and !circuit[peca_act].isSwitch() and not first) {
                chooseSwitch = false;
                vector<double> dist;
                vector<int> cami;
                dijkstra(graf, dist, cami, node_act);
                int i = peca_act;
                while (!circuit[(++i)%circuit.size()].isSwitch());
                // Primer switch
                int primer_sw = i%circuit.size();
                // Segon switch
                while (!circuit[(++i)%circuit.size()].isSwitch());
                int segon_sw = i%circuit.size();
                double mindist = numeric_limits<double>::infinity();
                int minlane = lane;
                for (int j = 0; j < lanes.size(); ++j) {
                    //cout << "\t" << dist[segon_sw*lanes.size() + j] << endl;
                    if (dist[(segon_sw*lanes.size() + j)%dist.size()] < mindist) {
                        minlane = j%lanes.size();
                        mindist = dist[(segon_sw*lanes.size() + j)%dist.size()];
                    }
                }
                //cout << "MINS:" << minlane << "  " << mindist << endl;
                if (minlane-lane == -1) {
                    cout << "Switch left goes" << endl;
                    return { make_switch("Left") };
                }
                else if (minlane-lane == 1) {
                    cout << "Switch right goes" << endl;
                    return { make_switch("Right") };
                }
            }

            // ... DO GET SOEM THROTTLE
            if (lTrobada) {
            	// find distance to stop before next piece
            	double minvel = min(velocitats[(peca_act)%circuit.size()][endLane], min(velocitats[(peca_act+2)%circuit.size()][endLane], velocitats[(peca_act+1)%circuit.size()][endLane]));
        		if (vel_act > minvel) {
        			if (abs(angle) > 40.0 || abs(angle) > ds1.angle_ant + 15.0 || vel_act-minvel > 0.25*k) {
        				throttle = 0.0;
        			}
            		else {
            			throttle = minvel / k;
            		}
        		}
            	else {
					if (abs(angle) > 50.0) throttle = 0.0;
					else {
						if (circuit[(peca_act+1)%circuit.size()].isStraight() and circuit[(peca_act+2)%circuit.size()].isStraight())
							throttle = 1.0;
						else throttle = min(minvel / k + 0.1, 1.0);
					}
            	}
            }

            else {
            	if (circuit[(peca_act+1)%circuit.size()].isStraight()) {
	                if (throttle == 0.4) throttle_ticks = 0;
	                throttle = 1.0;
	            }
	            else {
	                if (throttle == 0.8) throttle_ticks = 0;
	                throttle = 0.4;
	            }
            }
            

            ds1.vel_ant = vel_act;
            ds1.peca_ant = peca_act;
            ds1.dist_ant = inPieceDist;
            ds1.angle_ant = abs(angle);
            first = false;
        }
        else { // FUCK EVERYBODY -> LITERALLY, I MEAN, REALLY

        }
    }

    ++throttle_ticks;
    if (throttle_ticks > 5)
        return { make_throttle(throttle) };
    else
        return { make_throttle(1.0) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data) {
    // Algú ha petat
    std::string car_name = data["name"].as_string();
    std::cout << car_name << " crashed!" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data) {
    // Algú ha petat
    std::string car_name = data["name"].as_string();
    std::cout << car_name << " spawned!" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data) {
    // s'ha acabat la carrera
    std::cout << "Race ended" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data) {
    // BEEP BEEP MUDAFACKARS
    std::cout << "Error: " << data.to_string() << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data) {
    // El turbo està disponible
    std::cout << "Turbo Available" << std::endl;
    turboDurationTicks = data["turboDurationTicks"].as_int();
    turboDurationMilliseconds = data["turboDurationMilliseconds"].as_double();
    turboFactor = data["turboFactor"].as_double();
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data) {
    // S'inicia el turbo
    std::cout << "Turbo Start" << std::endl;
    turboDurationTicks = 0;
    turboDurationMilliseconds = 0;
    turboFactor = 0;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data) {
    // S'ha acabat el turbo
    std::cout << "Turbo End" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data) {
    // Algú acaba una volta
    std::string car_name = data["car"]["name"].as_string();
    int nlap = data["lapTime"]["lap"].as_int();
    int pos = data["ranking"]["overall"].as_int();
    double t = data["lapTime"]["millis"].as_double()/1000.0;
    std::cout << car_name << " finished lap " << nlap << "! Rank " << pos << " Time = " << t << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data) {
    // Cotxe desqualificat
    std::cout << data["car"]["name"].to_string() << " ha sigut desqualificat " << data["reason"].to_string() << std::endl;
    return { make_ping() };
}

void game_logic::dijkstra(const Graf& g, vector<double>& dist, vector<int>& cami, int s) {
    cami = vector<int> (g.size(),-1);
    dist = vector<double> (g.size(),std::numeric_limits<double>::infinity());
    minheap pq;
    dist[s] = 0.0;
    pq.push(PDI(0.0,s));
    vector<bool> S(dist.size(), false);
    
    while (not pq.empty()) {
        int act = pq.top().second;
        pq.pop();
        if (not S[act]) {
            S[act] = true;
            for (int i=0; i < g[act].size(); ++i) {
                int nn = g[act][i].second  ;
                double pes = g[act][i].first;
                
                if (dist[nn] > dist[act] + pes) {
                    dist[nn] = dist[act] + pes;
                    cami[nn] = act;
                    pq.push(PDI(dist[nn],nn));
                }
            }
        }
    }
}

double game_logic::getVelocity(int peca_act, int lane_act, double dist_act) {
    if (peca_act == ds1.peca_ant) {
        return dist_act-ds1.dist_ant;
    }
    else {
        int peca_ant = peca_act-1;
        if (peca_ant < 0) peca_ant = circuit.size() - 1;
        piece& pa = circuit[peca_ant];
        if (pa.isStraight()) {
            return dist_act + (pa.getLength() - ds1.dist_ant);
        }
        else {
            double dist_curva = (abs(pa.getAngle())*pi/180.0)*(pa.getRadius()-sgn(pa.getAngle())*lanes[lane_act]);
            return dist_act + (dist_curva - ds1.dist_ant);
        }
    }
}

void game_logic::calcula_velocitats_max() {
    velocitats = vector<vector<double>> (circuit.size(), vector<double> (lanes.size()));
    for (int i = 0; i < velocitats.size(); ++i) {
        for (int j = 0; j < velocitats[i].size(); ++j) {
            if (not circuit[i].isStraight()) velocitats[i][j] = sqrt(l*(circuit[i].getRadius()+(sgn(circuit[i].getAngle())*lanes[j])));
            else velocitats[i][j] = k;
        }
    }
}

template <typename T> double game_logic::sgn(T val) {
    return double((T(0) < val) - (val < T(0)));
}
